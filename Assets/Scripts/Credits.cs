﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Credits : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
		if (Input.GetKeyDown(KeyCode.Alpha0))
			UnityEngine.SceneManagement.SceneManager.LoadScene(0);
		if (Input.GetKeyDown(KeyCode.Alpha1))
			UnityEngine.SceneManagement.SceneManager.LoadScene(1);
		if (Input.GetKeyDown(KeyCode.Escape))
			Application.Quit();
	}
}
