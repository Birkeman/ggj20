﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RigidbodyResetter : MonoBehaviour
{
	Vector3 startPos;
	Quaternion startRot;

	Rigidbody2D body;

    void Start()
    {
		LevelManager.instance.onStateChanged += OnLevelStateChanged;

		startPos = transform.position;
		startRot = transform.rotation;

		body = GetComponent<Rigidbody2D>();
    }

	void OnLevelStateChanged(LevelManager.State state)
	{
		transform.position = startPos;
		transform.rotation = startRot;

		body.velocity = Vector3.zero;
		body.angularVelocity = 0;
	}
}
