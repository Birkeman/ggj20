﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SoundManager : MonoBehaviour
{

    public static SoundManager instance;

    [SerializeField]
    private AudioClip[] audioClips;
    [SerializeField]
    private AudioClip mainTheme;
    [SerializeField]
    private AudioClip ambience;
    [SerializeField]
    private AudioSource audioSource;
    [SerializeField]
    private AudioSource audioSourceSFX;
    [SerializeField]
    private AudioSource audioSourceAmbience;

    private void Awake()
    {
		if (instance == null)
		{
			instance = this;
			DontDestroyOnLoad(this.gameObject);
		}
		else
		{
			Destroy(gameObject);
		}

    }

    //play sound effect
    public void PlaySFX(string sfx)
    {
        foreach(AudioClip clip in audioClips)
        {
            if (clip.name == sfx)
            {
                //load audio into source and play
                audioSourceSFX.clip = clip;
                audioSourceSFX.Play();
            }

        }
    }

    //play music
    public void PlayMusic()
    {
        //play main theme
        audioSource.clip = mainTheme;
        audioSource.Play();
        audioSource.loop = true;
    }

    //stop music
    public void StopMusic()
    {
        //stop main theme
        audioSource.Stop();
    }

    //play ambience
    public void PlayAmbience()
    {
        //play ambience
        audioSourceAmbience.clip = ambience;
        audioSourceAmbience.Play();
        audioSourceAmbience.loop = true;
    }

    //stop ambience
    public void StopAmbience()
    {
        //stop main theme
        audioSourceAmbience.Stop();
    }
}
