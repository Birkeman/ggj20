﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeySprite : MonoBehaviour
{
	public TMPro.TextMeshPro text;
	public void SetText(KeyCode code)
	{
		text.text = code.ToString();
	}

	public void DestroyWhenDone()
	{
		Destroy(gameObject);
	}
}
