﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickAndDrag : MonoBehaviour
{
    private bool dragging = false;
    private bool attached = false;
    private bool HasNotBeenDragged = true;

	public int trayIndex;

	PlayerAppendage appendage;
	Rigidbody2D body;

	bool mouseOver;
	bool rotating;

	Collider2D[] colliders;

	Vector3 mouseWorldPos;

	Vector3 rotateStartPos;

    private void Awake()
    {
		appendage = GetComponent<PlayerAppendage>();

		body = GetComponent<Rigidbody2D>();

		body.isKinematic = true;

		colliders = GetComponentsInChildren<Collider2D>();

		LevelManager.instance.onStateChanged += OnLevelStateChanged;
	}

	void Update()
    {
	    var mousePos = Input.mousePosition;
		mouseWorldPos = Camera.main.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, 1));
		
		if(PositionInsideColliders(mouseWorldPos))
		{
			if (Input.GetMouseButtonDown(0))
			{
				if (attached)
				{
					PlayerBody.instance.RemoveAppendage(appendage);
					attached = false;
                    //random choice of attach sound
                    int randomNumber = Random.Range(1, 3);
                    //play detach sound
                    SoundManager.instance.PlaySFX("body_detach" + randomNumber);
                }
				else
				{
					if (HasNotBeenDragged)
					{
						PlayerBuilder.instance.SpawnAppendage(trayIndex);
						HasNotBeenDragged = false;
					}
				}

				//follow cursor variable true when mouse button held down
				dragging = true;
				PlayerBuilder.instance.draggingAppendage = true;
			}
			else if (Input.GetMouseButtonDown(1))
			{
				PlayerBody.instance.RemoveAppendage(appendage);
				rotating = true;
				rotateStartPos = mouseWorldPos;
                
            }
		}

		if (dragging)
        {
		   transform.position = mouseWorldPos;

            if (Input.GetMouseButtonUp(0))
            {
				//check mouse is over a lega placement area (the body)

				PlayerBuilder.instance.draggingAppendage = false;

				if (!PlayerCollider.instance.mouseOver)
                {
                    Destroy(this.gameObject);
                }
                else
                {
                    if (!attached)
                    {
                        PlayerBody.instance.AddAppendage(appendage);
						body.isKinematic = false;
						body.WakeUp();
						transform.parent = null;
                        //random choice of attach sound
                        int randomNumber = Random.Range(1, 3);
                        //play attach sound
                        SoundManager.instance.PlaySFX("body_attach" + randomNumber);

						//limbRB.GetComponentInChildren<PolygonCollider2D>().isTrigger = false;
						attached = true;
                    }
                }

                dragging = false;
            }
        }
		else if(rotating)
		{
			body.isKinematic = true;

			Vector3 toMouse = mouseWorldPos - rotateStartPos;
			if (toMouse.magnitude > 0.1f)
				transform.up = toMouse;

			if (Input.GetMouseButtonUp(1))
			{
				PlayerBody.instance.AddAppendage(appendage);	
				body.isKinematic = false;
				rotating = false;
			}
		}
    }

	bool PositionInsideColliders(Vector3 pos)
	{
		foreach(var col in colliders)
		{
			if (col.OverlapPoint(pos))
				return true;
		}

		return false;
	}


	private void OnDestroy()
	{
		LevelManager.instance.onStateChanged -= OnLevelStateChanged;
	}

	void OnLevelStateChanged(LevelManager.State state)
	{
		enabled = state == LevelManager.State.Build;
	}
}
