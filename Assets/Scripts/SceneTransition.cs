﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneTransition : MonoBehaviour
{
    private int sceneCount = 0;

    public void ToGame()
    {
        SceneManager.LoadSceneAsync(sceneCount + 1);
    }
}
