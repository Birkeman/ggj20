﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayScreen : MonoBehaviour
{
	void Awake()
	{
		LevelManager.instance.onStateChanged += OnLevelStateChanged;
	}

	private void Update()
	{
		if(Input.GetKeyDown(KeyCode.Escape))
		{
			LevelManager.instance.SetState(LevelManager.State.Build);
		}
	}

	void OnLevelStateChanged(LevelManager.State state)
	{
		gameObject.SetActive(state == LevelManager.State.Play);
	}
}
