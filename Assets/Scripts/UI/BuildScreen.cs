﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildScreen : MonoBehaviour
{
	void Awake()
	{
		LevelManager.instance.onStateChanged += OnLevelStateChanged;
	}

	void OnLevelStateChanged(LevelManager.State state)
	{
		gameObject.SetActive(state == LevelManager.State.Build);
	}

	public void PlayButtonClicked()
    {
		LevelManager.instance.SetState(LevelManager.State.Play);
	}
}
