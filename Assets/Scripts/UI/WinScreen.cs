﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinScreen : MonoBehaviour
{
	void Awake()
    {
		LevelManager.instance.onStateChanged += OnLevelStateChanged;        
    }

	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.Space))
			GoToNextScene();
	}

	void OnLevelStateChanged(LevelManager.State state)
    {
		gameObject.SetActive(state == LevelManager.State.Won);
    }

	public void GoToNextScene()
	{
		UnityEngine.SceneManagement.SceneManager.LoadScene(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex + 1);
	}
}
