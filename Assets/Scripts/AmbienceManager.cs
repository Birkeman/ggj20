﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmbienceManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Awake()
    {
        SoundManager.instance.PlayMusic();
        SoundManager.instance.PlayAmbience();
    }

}
