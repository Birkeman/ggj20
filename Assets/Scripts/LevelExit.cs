﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelExit : MonoBehaviour
{
	[SerializeField] float startDelay = 1f;
	[SerializeField] GameObject virtualCam;

	public bool doPan = true;

	private void Start()
	{
		if(doPan)
		{
			LevelManager.instance.SetState(LevelManager.State.Start);
			StartCoroutine(WaitThenPan());
		}
	}

	IEnumerator WaitThenPan()
	{
		virtualCam.SetActive(true);
		yield return new WaitForSeconds(startDelay);

		virtualCam.SetActive(false);
		LevelManager.instance.SetState(LevelManager.State.Build);
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if(collision.CompareTag("Player"))
		{
			LevelManager.instance.SetState(LevelManager.State.Won);
		}
	}
}
