﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animation))]
[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(ClickAndDrag))]
public abstract class PlayerAppendage : MonoBehaviour
{
	[Header("Debug")]
	public int keyIndex;
	public KeyCode keyCode;
	public bool attached;
    public string SFX;

	FixedJoint2D joint;

	private void Update()
	{
		if (LevelManager.instance.state != LevelManager.State.Play || !attached)
			return;

        if (Input.GetKeyDown(keyCode))
        {
            SoundManager.instance.PlaySFX(SFX);
            OnKeyDown();
        }

		if (Input.GetKeyUp(keyCode))
			OnKeyUp();

		if (Input.GetKey(keyCode))
			OnKeyHold();
	}

	protected virtual void OnKeyDown()
	{

	}
	protected virtual void OnKeyHold()
	{

	}
	protected virtual void OnKeyUp()
	{

	}

	public void Attach()
	{
		joint = gameObject.AddComponent<FixedJoint2D>();
		joint.connectedBody = PlayerBody.instance.body;
		attached = true;
	}

	public void Detach()
	{
		Destroy(joint);
		attached = false;
	}
}
