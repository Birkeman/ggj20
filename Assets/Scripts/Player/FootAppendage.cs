﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FootAppendage : PlayerAppendage
{
	[SerializeField] Animation anim;
	[SerializeField] float force = 100f;
	[SerializeField] Collider2D col;

	public float forceModifier = 0;

	private void FixedUpdate()
	{
		float f = force * forceModifier;

		if (!col.IsTouchingLayers(PlayerBody.instance.collisionMask))
			f = 0;

		PlayerBody.instance.body.AddForceAtPosition(transform.right * f, transform.position);
	}

	protected override void OnKeyDown()
	{
		anim.Stop();
		anim.Play();
	}
}
