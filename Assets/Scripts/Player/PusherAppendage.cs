﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PusherAppendage : PlayerAppendage
{
	[SerializeField] Animation anim;
	[SerializeField] float force = 100f;
	[SerializeField] Transform forcePivot;
	[SerializeField] Collider2D col;

	public void Push()
	{
		float f = force;
		
		if (!col.IsTouchingLayers(PlayerBody.instance.collisionMask))
			f = 0;

		PlayerBody.instance.body.AddForceAtPosition(transform.right * f, forcePivot.position, ForceMode2D.Impulse);
	}

	protected override void OnKeyDown()
	{
		anim.Stop();
		anim.Play();
	}
}
