﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBody : MonoBehaviour
{
	public new Rigidbody2D body;

	public KeySprite keySpritePrefab;

	public LayerMask collisionMask;

	public static PlayerBody instance;

	public KeyCode[] keys;

	List<int> availableIndices = new List<int>();

	Vector3 startPos;
	Quaternion startRot;

	List<PlayerAppendage> appendages = new List<PlayerAppendage>();

	private void Awake()
	{
		instance = this;
		LevelManager.instance.onStateChanged += OnLevelStateChanged;

		for(int i = keys.Length - 1; i >= 0; i--)
		{
			availableIndices.Add(i);
		}

		transform.position = PlayerBuilder.instance.playerStartPivot.position;
		startPos = transform.position;
		startRot = transform.rotation;
	}

	public void AddAppendage(PlayerAppendage appendage)
	{
		appendage.keyIndex = availableIndices[availableIndices.Count - 1];
		availableIndices.RemoveAt(availableIndices.Count - 1);
		appendage.keyCode = keys[appendage.keyIndex];

		appendage.Attach();

		appendages.Add(appendage);

		KeySprite keySprite = Instantiate(keySpritePrefab, appendage.transform.position, Quaternion.identity);
		keySprite.SetText(appendage.keyCode);
	}

	public void RemoveAppendage(PlayerAppendage appendage)
	{
		availableIndices.Add(appendage.keyIndex);

		appendage.Detach();

		appendages.Remove(appendage);
	}

	void OnLevelStateChanged(LevelManager.State state)
	{
		if (state == LevelManager.State.Build)
		{
			List<Vector3> positions = new List<Vector3>();
			List<Vector3> upDirections = new List<Vector3>();

			for (int i = 0; i < appendages.Count; i++)
			{
				positions.Add(transform.InverseTransformPoint(appendages[i].transform.position));
				upDirections.Add(transform.InverseTransformDirection(appendages[i].transform.up));
			}

			transform.position = startPos;
			transform.rotation = startRot;

			for (int i = 0; i < appendages.Count; i++)
			{
				appendages[i].transform.position = transform.TransformPoint(positions[i]);
				appendages[i].transform.up = transform.TransformDirection(upDirections[i]);
			}

			body.isKinematic = true;
			body.velocity = Vector2.zero;
			body.angularVelocity = 0;
		}
		else if (state == LevelManager.State.Play)
		{
			body.isKinematic = false;
		}
		else
		{
			body.isKinematic = true;
			body.velocity = Vector2.zero;
			body.angularVelocity = 0;
		}

		print("state " + state);
	}
}
