﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CosmeticAppendage : PlayerAppendage
{
	new Animation aniamtion;

	private void Start()
	{
		aniamtion = GetComponent<Animation>();
		aniamtion.playAutomatically = false;
		aniamtion.Stop();
	}

	protected override void OnKeyDown()
	{
		base.OnKeyDown();

		aniamtion.Stop();
		aniamtion.Play();
	}
}
