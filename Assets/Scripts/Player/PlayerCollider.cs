﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollider : MonoBehaviour
{
	public bool mouseOver;
	public GameObject hoverSprite;

	public static PlayerCollider instance;

	new Collider2D collider;
		void Awake()
    {
		instance = this;
		collider = GetComponent<Collider2D>();
    }

	private void Update()
	{
		Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		mouseOver = collider.OverlapPoint(mousePos);

		hoverSprite.SetActive(mouseOver && PlayerBuilder.instance.draggingAppendage);
	}

	void OnLevelStateChanged(LevelManager.State state)
	{
		enabled = state == LevelManager.State.Build;
		mouseOver = state == LevelManager.State.Build ? mouseOver : false;
		hoverSprite.SetActive(mouseOver);
	}

}
