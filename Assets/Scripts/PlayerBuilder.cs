﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBuilder : MonoBehaviour
{
	[SerializeField] PlayerAppendage[] appendagePrefabs;

	public Transform playerStartPivot;

	[Header("Spawning")]
	public GameObject buildSpecific;
	public Transform spawnPivot;
	public Vector3 spawnOffset;

	public static PlayerBuilder instance;

	public bool draggingAppendage;
	private void Awake()
	{
		instance = this;
	}

	void Start()
    {
		LevelManager.instance.onStateChanged += OnLevelStateChanged;

		for(int i = 0; i < appendagePrefabs.Length; i++)
		{
			SpawnAppendage(i);
		}
    }

	public void SpawnAppendage(int index)
	{
		Vector3 pos = spawnPivot.position;
		pos.y += (index / 2) * spawnOffset.y;
		pos.x += (index % 2) * spawnOffset.x;

		PlayerAppendage appendage = Instantiate(appendagePrefabs[index], pos, Quaternion.identity);
		appendage.GetComponent<ClickAndDrag>().trayIndex = index;
		appendage.transform.SetParent(spawnPivot, true);
	}

	void OnLevelStateChanged(LevelManager.State state)
	{
		buildSpecific.SetActive(state == LevelManager.State.Build);
	}
}
