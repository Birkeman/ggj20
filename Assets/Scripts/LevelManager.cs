﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
	public static LevelManager instance;

	public State initialState = State.Build;

	public enum State { Build, Play, Start, Won}

	public State state { get; private set; }

	public event System.Action<State> onStateChanged = delegate { };

    void Awake()
    {
		instance = this;
    }

	private void Start()
	{
		SetState(initialState);
	}

	public void SetState(State state)
	{
		this.state = state;
		onStateChanged(state);

	//	Physics2D.autoSimulation = state != State.Build;
	}	
}
